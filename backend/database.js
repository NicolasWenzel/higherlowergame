//init sqlite3
const sqlite3 = require('sqlite3').verbose();

//defining database name
const dbName = "higherlowergamedatabase.db"

// creating database object
let db = new sqlite3.Database(dbName, (err) => {
    if (err) {
        // Cannot open database
        console.error(err.message)
        throw err
    }else{
        // create tables if not exist
        console.log('Connected to the SQLite database.')
        db.run('CREATE TABLE IF NOT EXISTS t_user (\n' +
            '\tuser_id INTEGER PRIMARY KEY AUTOINCREMENT ,\n' +
            '   user_name TEXT NOT NULL\n' +
            ')');

        db.run('CREATE TABLE IF NOT EXISTS t_highscore (\n' +
            '\tscore_id INTEGER PRIMARY KEY AUTOINCREMENT,\n' +
            '   user_id INTEGER NOT NULL,\n' +
            '   score INTEGER NOT NULL DEFAULT 0,\n' +
            '   FOREIGN KEY (user_id) REFERENCES t_user(user_id)' +
            ')');
    }
});

//export database object for use in server.js
module.exports = db;








