// Create express app
const express = require("express");
const db = require("./database.js");
const http = require('http');
const app = express();
const router = express.Router();
// Use Cross Origin Resource Sharing
const cors = require('cors');
app.use(cors());
// Use router
app.use(router);
// disable etag for request status
app.disable('etag');
//create http server
const httpServer = http.createServer(app);

// Server port
let HTTP_PORT = 8008
// Start server
httpServer.listen(HTTP_PORT, () => {
    console.log("Server running on port"+HTTP_PORT)
});


// Root endpoint
router.get("/", (req, res) => {
    res.json({"message":"Welcome"})
});

// Database API URL Requests

//get top highscores
router.get("/db/topscores/", (req, res) => {
    let topscores = [];
    let maxScores = req.query.max;
    // execute database query for selecting user score, user name sorted by the score in descending order and limited by the query parameters value
    let sql = "SELECT t_h.score, t_u.user_name FROM t_highscore AS t_h JOIN t_user  AS t_u ON t_h.user_id = t_u.user_id ORDER BY t_h.score DESC LIMIT "+maxScores;
    db.all(sql, [], (err, rows) => {
        if (err) {
            throw err;
        }
        let rank = 1;
        // put all rows in topscores array with additional rank value
        rows.forEach((row) => {
            topscores.push({'user_name': row['user_name'], 'score': row['score'], 'rank': rank});
            rank++;
        });
        res.send({topscores});
    });
});



//insert user
router.get("/db/insert-user",  (req, res, next) => {
    let sql = 'INSERT INTO t_user(user_name) VALUES(?)'
    let params =[req.query.name]
    console.log(params)
    //execute database query for inserting user
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            //returning last inserted user_id
            "id" : this.lastID
        })
    });
})

//insert user
router.get("/db/insert-highscore",  (req, res, next) => {
    let sql = 'INSERT INTO t_highscore(user_id, score) VALUES(?, ?)';
    let params =[req.query.id, req.query.score];
    console.log(params)
    //execute database query for inserting highscore
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            // return success message
            "message": "success",
        })
    });
})

//method for retrieving user rank based on his score
router.get("/db/user-rank",  (req, res, next) => {
    let allscores = [];
    let user_id = req.query.id;
    // query for selecting all highscores ordered by the score value in descending order
    let sql = ' SELECT * FROM t_highscore ORDER BY score DESC';
    db.all(sql, [], (err, rows) => {
        if (err) {
            throw err;
        }
        // store all scores in an array
        rows.forEach((row) => {
            allscores.push(row);
        });
        //init default user rank value
        let user_rank = 1000;
        //loop through all scores to find the matching score
        for(let i = 0; i < allscores.length; i++){
            let entry = allscores[i];
            //assigning rank the value of i and increasing it by one due to the zero based index
            let rank = i;
            rank++;
            console.log(entry['user_id'])
            //when the user id matches the id from the request parameter id assign the counted rank to user_rank
            if(entry['user_id'] == user_id){
                user_rank = rank;
            }
        }
        //send user rank as response
        res.send({"rank":user_rank});
    });
})

// Default response
app.use(function(req, res){
    res.status(404);
});

