import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';

import { MatCardModule} from "@angular/material/card";

import { HttpClientModule} from '@angular/common/http';
import { GameComponent } from './game/game.component';

import { Globals } from './globals';
import {FormsModule} from "@angular/forms";

import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";

const modules = [
  MatButtonModule,
  MatCardModule,
  HttpClientModule,
  MatTableModule,
  MatInputModule
];


@NgModule({
  declarations: [
    AppComponent,
    ScoreboardComponent,
    GameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    modules,
    FormsModule,
    MatProgressSpinnerModule
  ],
  providers: [Globals],
  bootstrap: [AppComponent]
})
export class AppModule { }
