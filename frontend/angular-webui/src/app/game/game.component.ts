import {Component, OnInit} from '@angular/core';
import {EbayService, Product} from '../ebay.service';
import {Router} from '@angular/router';
import { Globals } from '../globals';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
})
export class GameComponent implements OnInit {

  //define class variables
  globals: Globals;
  leftProd: Product;
  rightProd: Product;
  currentScore: number;
  isRight: boolean;
  isWrong: boolean;


  constructor(private ebayService: EbayService, private router: Router, globals: Globals) {
    this.globals = globals;
  }

  //init component
  ngOnInit(): void {
    this.globals.isDisplay = true;
    //set left and right product for game
    this.setLeftAndRightProduct();
    //init score with 0
    this.currentScore = 0;
  }

  //method for retrieving a single product from our product list
  getProduct(): any {
    let product = new Product();
    // getting randomIndex based on the length of the product list
    let randomIndex = Math.floor(Math.random() * this.globals.productList.length);
    // get one product list from our all product lists by randomIndex
    let data = this.globals.productList[randomIndex];
    // get the item count of the selected product list
    let itemCount = data["findItemsByKeywordsResponse"][0]["searchResult"][0]["@count"];
    // get random item index
    let randomItemIndex = Math.floor(Math.random() * itemCount);
    // retrieve one product based on the random item index
    let items = data["findItemsByKeywordsResponse"][0]["searchResult"][0]["item"];
    let item = items[randomItemIndex];
    // as long as the item is an item that does not contain a super size picture, find a new item
    while (!item["pictureURLSuperSize"]) {
      randomItemIndex = Math.floor(Math.random() * itemCount);
      item = data["findItemsByKeywordsResponse"][0]["searchResult"][0]["item"][randomItemIndex];
    }
    // assign all the values we need for the game to the product object
    product.price = item["sellingStatus"][0]["currentPrice"][0]["__value__"];
    product.currency = item["sellingStatus"][0]["currentPrice"][0]["@currencyId"];
    product.title = item["title"];
    product.galleryURL = item["pictureURLSuperSize"];

    // return the final product
    return product;
  }


  //method for setting the left and right product
  setLeftAndRightProduct() {
    this.leftProd = this.getProduct();
    this.rightProd = this.getProduct();
  }

  //method for checking if the user guess was correct
  guessPrice(userGuessHigher: boolean) {
    // find out which price is higher
    let subtractedPrice = this.leftProd.price - this.rightProd.price;
    let isHigher = subtractedPrice < 0;

    //if the user guess matches the isHigher boolean or if both prices are the same = subtracted Price is zero
    // then keep playing
    if (userGuessHigher == isHigher || subtractedPrice == 0) {
      this.isRight = true;
      //timeout for a comfortable game feeling
      setTimeout(()=>{
        this.isRight = false;
   }, 1500);
      //put the right product to the left side for next round
      this.leftProd = this.rightProd;
      this.currentScore++;
      // get a new product for the right side
      this.rightProd = this.getProduct();
      // when the user guess is wrong end the game
    } else {
      this.isWrong = true;
      //another timeout for a nicer feeling
      setTimeout(()=>{
        this.isWrong = false;
   }, 2000);
      console.log("CurrentScore: " + this.currentScore);
      //store the users reached score in the global score variable
      this.globals.score = this.currentScore;
      //navigate to the scoreboard
      setTimeout(()=>{
        this.router.navigate(['scoreboard']);
   }, 2000);
    }
  }


}

