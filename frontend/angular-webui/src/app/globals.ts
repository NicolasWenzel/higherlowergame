import {Injectable} from '@angular/core';

@Injectable()
export class Globals {
  name: string;
  score: number = 0;
  isDisplay: boolean;
  productList: any;
}
