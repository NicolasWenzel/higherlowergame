import {Component, Injectable, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../globals';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';


@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})


@Injectable()
export class ScoreboardComponent implements OnInit {


  constructor(private activatedRoute: ActivatedRoute, private http: HttpClient, globals: Globals, private router: Router, private table: MatTableModule, private input: MatInputModule) {
    this.globals = globals;
  }

  globals: Globals;
  // Databse Backend API Request URLs
  insertUserURL = "http://localhost:8008/db/insert-user?name=";
  insertHighscoreURL = "http://localhost:8008/db/insert-highscore?id=";
  selectTopHighscoresURL = "http://localhost:8008/db/topscores?max=";
  selectUserRankURL = "http://localhost:8008/db/user-rank?id=";

  //define class variables
  topHighscores: [];
  userRank = 0
  submitIsShown: boolean;
  rankIsShown: boolean;

  //displayed columns for angular table
  displayedColumns: string[] = ['rank', 'user_name', 'score'];

  // Initialize component
  ngOnInit(): void {
    this.globals.isDisplay = true;
    this.submitIsShown = true;
    this.rankIsShown = false;
    //Get current top Highscores
    this.selectTopHighscores(20).subscribe(result => {
      this.topHighscores = result["topscores"];
    });
  }

  //Submit User Highscore into database
  submitUserHighscore(): any {
    //Only submit when the name field is not empty
    if (this.globals.name.trim().length > 0) {
      //first insert the user
      this.http.get(this.insertUserURL + this.globals.name).subscribe(result => {
        // retrieve the generated user_id
        let lastUserId = result['id'];
        // insert highscore with the retrieved user_id
        this.insertHighscore(lastUserId, this.globals.score).subscribe(result => {
          // retrieve new list of top highscores in case the user is now added to the scoreboard
          this.selectTopHighscores(20).subscribe(result => {
            this.topHighscores = result["topscores"];
          });
          // retrieve the user rank of the inserted score
          this.selectUserRank(lastUserId).subscribe(result => {
            this.userRank = result['rank'];
            this.submitIsShown = false;
            this.rankIsShown = true;
            // window.location.reload();
          });
        });
      });
    }
  }

  resetValues() {
    // reset global variables
    this.globals.score = 0;
    this.globals.name = "";
  }

  //returning http request for inserting highscore
  insertHighscore(user_id: number, highscore: number): any {
    return this.http.get(this.insertHighscoreURL + user_id + "&score=" + highscore);
  }

  // returning http request for selecting top highscores
  selectTopHighscores(maxScores: number): any {
    return this.http.get(this.selectTopHighscoresURL + maxScores);
  }

  // returning http request for user rank
  selectUserRank(user_id: number): any {
    return this.http.get(this.selectUserRankURL + user_id);
  }

}
